# Data importer for Huishoudboekje

## Using this chart
This chart can be installed by using the package repository, similar to the Huishoudboekje application.

It expects Huishoudboekje to be installed using the Helm charts in the same repository. If this is the case, the only
requirements are:

* `hhbReleaseName`: this a value required to be able to obtain a JWT. It extracts the JWT secret from the Huishoudboekje
    Kubernetes Secret containing it and uses this and other information from the release to generate a valid JWT.
    This JWT is required to be able to import data into Huishoudboekje.
* `importerDatasource`: can be set to `url` or `file`. By default, it is set to URL and will import the sample test
    data. It is possible to supply a custom URL to load JSON from by setting the `importerDatasourceUrl` value. If set
    to `file`, it is required to also set the `importerDatasourceFile` value which should contain valid JSON.

See the [`values.yaml`](./values.yaml) file for examples.
