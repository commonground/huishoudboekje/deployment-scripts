# huishoudboekje-data-importer-helm

## 2.0.1

### Changes
- None

### Bugfixes
- [#78](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/78) Typo in environment variable to
  determine number of organizations to generate.

## 2.0.0

### Changes
- [#77](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/77) Remove JWT-CLI as the new data-importer
 talks directly to the databases instead of to the application.
- [#77](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/77) Add secret lookup for Partyservice,
  Postadressenservice and Huishoudboekjeservice
- [#77](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/77) Add option to generate data and to
  specify number of organisations and citizens to generate and import
- Do not use a PVC to store JSON but use emptyDir volume

### Bugfixes
- None

## 1.3.0

### Changes
- [#26](https://gitlab.com/commonground/huishoudboekje/helm-charts/-/issues/26) Allow usage of self-supplied secrets
- Remove Kubernetes ServiceAccount as it is not needed.
- Change email address data-importer uses in JWT.
- Update JWT-CLI default version to 6.0.0.
- Update default data-importer version to 1.5.4.

### Bugfixes
- Fix YAML indentation error when using file data source.

### Contributors for this release
@tomwiggers

## 1.2.0

### Changes
- Update default data-importer version to 1.5.2.
- Make log level configurable.

### Bugfixes
- None

### Contributors for this release
@tomwiggers

## 1.1.0

### Changes
- Update default data-importer version from 1.1.0 to 1.5.1.

### Bugfixes
- Fix chart type in [`Chart.yaml`](./Chart.yaml) which had been accidentally batch replaced.

### Contributors for this release
@tomwiggers
