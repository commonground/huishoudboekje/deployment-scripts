# Huishoudboekje Helm chart repository

This repository contains the Huishoudboekje Helm chart to install the application. Inside the `huishoudboekje` is
another README with more information about the Helm chart and how to use it.

# Releases
The Helm chart used to be released on GitLab under `Deployments` --> `Releases`. This is now historical: the newer
versions are released as package under `Packages and registries` --> `Package Registry`.

# Installing
To make an HHB install, the repository where the packages are stored needs to be added first:
`helm repo add huishoudboekje https://gitlab.com/api/v4/projects/35817134/packages/helm/stable`

The Helm charts can then be used to install the Huishoudboekje application on a k8s cluster as follows. Make sure to
take the most recent version of the charts:

`helm install releaseName huishoudboekje/huishoudboekje --atomic --wait-for-jobs -f custom-values.yaml`

# One time jobs
The `migration-jobs` directory contains jobs (also in the form of Helm charts) meant to run once. These can be scripts
to fix issues or perform one-time database migrations. See the README in that directory for more information.
