{{/*
Generate templates for KeyCloak admin authentication.
*/}}

{{/*
Template function to lookup a value in a user provided secret and return it.
*/}}
{{- define "_getExistingAuthSecretValue" -}}
{{- $resource := ((lookup "v1" "Secret" .context.Release.Namespace .context.Values.existingSecrets.authentication.name).data | default dict) -}}
{{- printf "%s" (get $resource .info | b64dec) -}}
{{- end -}}

{{- define "keycloak.username" -}}
  {{- coalesce (include "_getExistingAuthSecretValue" (dict "context" . "info" .Values.existingSecrets.authentication.username)) .Values.authentication.username -}}
{{- end -}}

{{- define "keycloak.password" -}}
  {{- coalesce (include "_getExistingAuthSecretValue" (dict "context" . "info" .Values.existingSecrets.authentication.password)) .Values.authentication.password -}}
{{- end -}}
