---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "redis.fullname" . }}
  labels:
    {{- include "redis.labels" . | nindent 4 }}
  annotations:
    checksum/secrets: {{ include (print $.Template.BasePath "/secrets.yaml") . | sha256sum }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "redis.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "redis.selectorLabels" . | nindent 8 }}
    spec:
      enableServiceLinks: false
      automountServiceAccountToken: false
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ coalesce .Values.image.tag }}"
          imagePullPolicy: "{{ coalesce .Values.image.pullPolicy .Values.global.image.pullPolicy }}"
          command: [ redis-server ]
          args:
            - /config/redis.conf
          ports:
            - name: redis
              containerPort: 6379
              protocol: TCP
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
            - mountPath: /config/redis.conf
              name: config
              subPath: redis.conf
            - mountPath: /data
              name: data
          env:
            {{- if .Values.extraEnvVars }}{{- toYaml .Values.extraEnvVars | nindent 12 }}{{- end }}
      volumes:
        - name: config
          secret:
            secretName: {{ printf "%s-redis-config" .Release.Name }}
        - name: data
          {{- if .Values.persistence.enabled }}
          persistentVolumeClaim:
            claimName: {{ include "redis.fullname" . }}
          {{- else }}
          emptyDir: {}
          {{- end }}
