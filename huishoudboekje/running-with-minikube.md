# Running with Minikube

```bash
$ minikube delete # delete old cluster
🔥  Deleting "minikube" in docker ...
🔥  Deleting container "minikube" ...
🔥  Removing /home/opvolger/.minikube/machines/minikube ...
💀  Removed all traces of the "minikube" cluster.
$ minikube start # create a new cluster
😄  minikube v1.25.2 on Arch 21.2.5
✨  Automatically selected the docker driver. Other choices: kvm2, virtualbox, ssh
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=7900MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
$ minikube addons enable ingress # enable plugin ingress
    ▪ Using image k8s.gcr.io/ingress-nginx/controller:v1.1.1
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.1.1
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.1.1
🔎  Verifying ingress addon...
🌟  The 'ingress' addon is enabled
$ minikube ip # get the ip of your cluster
192.168.49.2
```

At this point, we can test the Ingress to the local cluster. To do this, the cluster IP and given hostname needs to be
added to the `hosts` file.

## MacOS and Linux

```bash
$ sudo nano /etc/hosts
```

## Windows

Make the following changes to `C:\Windows\System32\drivers\etc\hosts`

```ini
192.168.49.2 huishoudboekje.sloothuizen.nl
```

Create a value file for minikube `values_minikube.yaml`:

```yaml
---
backend:
  # Minikube will not use a valid SSL certificate out of the box, so we need http here.
  frontend_scheme: http
  # The backend connects over the Keycloak-service's hostname.
  keycloak_address: "[release-name]-keycloak"
  # Use development profile instead of production profile.
  profile: LocalConfig
  oidc:
    # Connect to the keycloak endpoint URLs using http instead of https.
    endpoints_scheme: http

keycloak:
  scheme: http
  authentication: # These are the credentials for the admin user. Default username is admin.
    password: "Mak3UpY0ur0wnP@ssword1234"

global:
  keycloak:
    enabled: true
    # You can choose to add additional users that will be loaded into Keycloak. 
    # The format: split on ':' and the user-strings are split on ','.
    # Example: username1,emailadres1@test.nl,firstname1,lastname1,password1:username2,emailadres2@test.nl,firstname2,lastname2,password2
    client_users: ""
```

Now create a namespace:

```bash
$ kubectl create namespace huishoudboekje
namespace/huishoudboekje created
$ helm install huishoudboekje_demo . -n huishoudboekje --values values_minikube.yaml
```

Open your browser to `http://huishoudboekje.sloothuizen.nl/`