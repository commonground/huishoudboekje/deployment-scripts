# Wrong Afpsraak Alarm fix job
Huishoudboekje Helm chart version must be at least . Includes check to make sure DB secret is correct.

## Example usage:
To install and run the job (assuming the GitLab Helm repository is present under the name "huishoudboekje"):
`helm install wrong-afspraak-alarm-fix-job huishoudboekje/wrong-afspraak-alarm-fix --atomic --wait-for-jobs --set hhbReleaseName=hhb-sloothuizen-acc`

By default the job only executes a check. To also execute the fix script, add the following argument to the
install/upgrade command: `--set runFix=true`. To optionally disable the check script, add: `--set runCheck=false`.

The job can be run without the funFix/runCheck values provided, it will then only execute check. If the check is OK, a
Helm upgrade can be done to execute the fix script.

To uninstall when done:
`helm uninstall wrong-afspraak-alarm-fix-job`
